import 'https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js'

const documentReady = () => {
    if (document.readyState === 'complete') {
	initWebsite()
    }
}

const initWebsite = () => {
    console.log('polymath.vision!')
    const $site = document.querySelector('.Site')
    $site.classList.remove('is-noJs')
    setupImagesLoaded($site)
    /* setTimeout(() => {
     * }, 3000) */
}

const setupImagesLoaded = ($element) => {
    imagesLoaded($element, function( instance ) {
	$element.classList.add('is-imagesLoaded')
    });
}

document.onreadystatechange = documentReady

export default {}
